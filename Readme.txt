Please Read These Three(3) Documents Prior to Using Haunter in any way:
        COPYING.txt (http://haunter.vortexborn.com/COPYING.txt)
        EULA.txt (http://haunter.vortexborn.com/EULA.txt)
        CREDITS.txt (http://haunter.vortexborn.com/CREDITS.txt)
        Thank you! :)

Haunter v1.0 Beta Release 1:
        Release Description:
                This release was more or less a rushed release, since as of Monday(7-12-08),
                I begin working at a new job I acquired. So time was short.
                It has MANY wonderful features. I have debugged them all to a great extent,
                and the only features that are buggy, have a notice of some kind stating that
                they are under construction.

        Usage:
                1) Run the Deploy.exe Utility on any computer
                2) Retreat to your own computer, and open the hc.exe Client Utility
                3) Enter the person's IP Address, and click Connect.
                4.) Have tons of fun. :)


        Known bugs: 
                "Random" Connection(Listening for reply) Issue ->
                        I need to isolate when this happens, find the culprate, and fix this.
                        Seems to happen when the Disconnect button is used in conjunction with other
                        GUI features, such as the Process Commander.
                        An attempt to fix this was made, but since removed. It "timed out" when
                        the client was listening for a response too long, and not recieving a reply.
                        But when long sounds were played, it timed out because the sound was still playing
                        and the message was not dispatched from server yet. Find a resolution!
                        ** SEE NOTE 1
                When server<->Client communication channel is interrupted
                        (ie, when server closes or net goes out), the program
                        infinite loops trying to connect. Fix this
                Web Sounds->
                        Needs to be finished and implemented. (ie INetGet Downloader - serverside)
                ComboBox Crash->
                        When you type text in combo box and press its corresponding button,
                        it crashes -> disable text input to combo boxes
                CTRL-Alt-Del/Win Keys Issue->
                        Find a way to unbind these from windows, so that the user cannot resume control
                        of their system... (Possibly not do-able with AutoIt programming.)
        Notes:
                NOTE1: 
                        This corresponding issue, may have been resolved already. If you encounter it
                        please contact me using the below contact information.
--------------------------------------------------------------
Contact:
        First try <http://haunter.vortexborn.com>
        Otherwise, you may email me: haunter@vortexborn.com